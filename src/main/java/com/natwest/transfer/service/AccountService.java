package com.natwest.transfer.service;

import com.natwest.transfer.model.Account;

public interface AccountService {
	
	Account createAccount(Account account);
	
	Account getAccount(long accountNumber);
	
	Account saveAccount(Account account);
	
	void deleteAccount(Account account);

	Account getAccountPessimistic(long accountNumber);

}
