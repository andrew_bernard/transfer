package com.natwest.transfer.service;

public interface TransferService {

	void transfer(long source, long destination, double amount);
	
	double getBalance(long accountNumber);
	
	
}
