package com.natwest.transfer.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.natwest.transfer.model.Account;
import com.natwest.transfer.repository.AccountRepository;

@Component
public class AccountServiceImpl implements AccountService {
	
	@Autowired
	AccountRepository accountRepo;

	@Override
	public Account createAccount(Account account) {
		return accountRepo.save(account);
	}

	@Override
	@Transactional
	public Account getAccountPessimistic(long accountNumber) {
		return accountRepo.findByAccountNumber(accountNumber);
	}
	
	public Account getAccount(long accountNumber) {
		return accountRepo.findByAccountNumberRead(accountNumber);
	}

	@Override
	public Account saveAccount(Account account) {
		return accountRepo.saveAndFlush(account);
	}

	@Override
	public void deleteAccount(Account account) {
		// TODO Auto-generated method stub

	}

}
