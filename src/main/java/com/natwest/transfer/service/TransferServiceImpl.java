package com.natwest.transfer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.natwest.transfer.model.Account;
import com.natwest.transfer.repository.AccountRepository;

@Component
public class TransferServiceImpl implements TransferService {
	
	@Autowired
	AccountRepository accountRepo;

	@Override
	public void transfer(long source, long destination, double amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getBalance(long accountNumber) {
		return getAccount(accountNumber).getBalance();
	}
	
	private Account getAccount(long accountNumber) {
		return accountRepo.findByAccountNumber(accountNumber);
	}

}
