package com.natwest.transfer.repository;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import com.natwest.transfer.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
	
	@Query(value = "select * from account a where a.account_number = :accountNumber", nativeQuery = true)
	Account findByAccountNumberRead(long accountNumber);
	
	@Lock(LockModeType.PESSIMISTIC_READ)
	Account findByAccountNumber(long accountNumber);

}
