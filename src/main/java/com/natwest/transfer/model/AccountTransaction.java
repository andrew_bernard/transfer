package com.natwest.transfer.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;

public class AccountTransaction {
	
	@NotEmpty
	private Long source;
	
	@NotEmpty
	private Long destination;
	
	@PositiveOrZero(message = "must be more than 0")
	private double amount;

	public Long getSource() {
		return source;
	}

	public void setSource(Long source) {
		this.source = source;
	}

	public Long getDestination() {
		return destination;
	}

	public void setDestination(Long destination) {
		this.destination = destination;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	

}
