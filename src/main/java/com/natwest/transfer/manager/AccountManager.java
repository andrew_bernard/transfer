package com.natwest.transfer.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.natwest.transfer.model.Account;
import com.natwest.transfer.model.AccountTransaction;
import com.natwest.transfer.service.AccountService;
import com.natwest.transfer.service.TransferService;

@Component
public class AccountManager {
	
	@Autowired
	TransferService transferService;
	
	@Autowired
	AccountService accountService;
	
	private boolean withdraw(Account account, double amount) {
		double newBalance = account.getBalance() - amount;
		account.setBalance(newBalance);
		return true;
	}
	
	private boolean deposit(Account account, double amount) {
		double newBalance = account.getBalance() + amount;
		account.setBalance(newBalance);
		return true;
	}
	
	public void transfer(AccountTransaction accountTransaction) throws Exception {
		isSourceSameAsDestination(accountTransaction);
		
		Account sourceAccount = accountService.getAccount(accountTransaction.getSource());
		Double amount = accountTransaction.getAmount();
			if(isBalanceEnough(sourceAccount, amount)) {
				withdraw(sourceAccount, amount);
			} else {
				throw new Exception("Cannot complete transaction");
			}
		
		Account destinationAccount = accountService.getAccount(accountTransaction.getDestination());
			deposit(destinationAccount, amount);
		
		accountService.saveAccount(sourceAccount);
		accountService.saveAccount(destinationAccount);
		
	}
	
	private boolean isSourceSameAsDestination(AccountTransaction accountTransaction) throws Exception {
		if(accountTransaction.getSource().equals(accountTransaction.getDestination())) {
			throw new Exception("Cannot complete transaction");
		}
		
		return false;
	}
	
	private boolean isBalanceEnough(Account account, double amount) {
		return account.getBalance() >= amount;
	}

}
