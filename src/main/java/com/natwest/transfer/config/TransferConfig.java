package com.natwest.transfer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.natwest.transfer.service.TransferService;
import com.natwest.transfer.service.TransferServiceImpl;

@Configuration
public class TransferConfig {
	
	@Bean
	public TransferService transferService() {
		return new TransferServiceImpl();
	}

}
