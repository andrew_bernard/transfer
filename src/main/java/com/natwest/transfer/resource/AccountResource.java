package com.natwest.transfer.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.natwest.transfer.manager.AccountManager;
import com.natwest.transfer.model.Account;
import com.natwest.transfer.model.AccountTransaction;
import com.natwest.transfer.service.AccountService;

@RestController
@RequestMapping("account")
public class AccountResource {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	AccountManager accountManager;
	
	@PostMapping(produces = "application/json")
	public Account createAccount(@RequestBody Account account) {
		try {
			return accountService.createAccount(account);
		}
		catch(Exception ex) {
			 throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, "Cannot create Account", ex);
		}
		
	}
	
	@GetMapping(value = "/{accountNumber}")
	public Account getAccount(@PathVariable Long accountNumber) {
		Account account = accountService.getAccount(accountNumber);
		if(account == null) {
			 throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, "Account Not Found");
		}
		return account;
	}
	
	@PostMapping(value = "/transfer")
	public void transfer(@RequestBody AccountTransaction accountTransaction) {
		try {
			accountManager.transfer(accountTransaction);
		} catch(Exception ex) {
			 throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, "Cannot create Account", ex);
		}
	}

}
