package com.natwest.transfer.resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.server.ResponseStatusException;

import com.natwest.transfer.model.Account;
import com.natwest.transfer.model.AccountTransaction;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AccountResourceTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	private String accountEndpoint;

	private String transferEndpoint;

	private HttpHeaders headers;

	private final long ACCOUNT_NUMBER_1 = 123;

	private final long ACCOUNT_NUMBER_2 = 456;

	@BeforeEach
	public void setup() throws JSONException {
		accountEndpoint = "http://localhost:" + port + "/account";
		transferEndpoint = accountEndpoint + "/transfer";

		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

	}

	@Test
	void transferTest() throws JSONException {

		// GIVEN Account 1 is created
		assertNotNull(createAccount(createJSONAccountObject(ACCOUNT_NUMBER_1, 100.0)).getId());

		// AND account 2 is created
		assertNotNull(createAccount(createJSONAccountObject(ACCOUNT_NUMBER_2, 100.0)).getId());

		// WHEN we transfer 100 from account 1 to account 2
		transfer(ACCOUNT_NUMBER_1, ACCOUNT_NUMBER_2, 100.00);

		// THEN account 1 balance should be 0
		assertEquals(Double.valueOf(0), getAccount(ACCOUNT_NUMBER_1).getBalance());

	}

	//@Test
	//void createAccountFail() throws JSONException {
		// test for 400 response code
		//assertThrows(ResponseStatusException.class, ()-> createAccount(createJSONAccountObject(ACCOUNT_NUMBER_2, -1.0)));
	//}
	
	@Test
     void concurrentTransfer() throws JSONException {
		Long account1 = Long.valueOf(789);
		Long account2 = Long.valueOf(980);
		
		// GIVEN Account 1 is created
		assertNotNull(createAccount(createJSONAccountObject(account1, 100.0)).getId());

		// AND account 2 is created
		assertNotNull(createAccount(createJSONAccountObject(account2, 100.0)).getId());
				
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
            	// WHEN we transfer 100 from account 1 to account 2
        		try {
					transfer(account1, account2, 100.00);
				} catch (JSONException e) {
					e.printStackTrace();
				}
            }).start();
        }
        
     // THEN account 1 balance should be 0
     		assertEquals(Double.valueOf(0), getAccount(account1).getBalance());

    }

	private void transfer(Long source, Long destination, Double amount) throws JSONException {
		JSONObject transaction = new JSONObject();
		transaction.put("source", source);
		transaction.put("destination", destination);
		transaction.put("amount", amount);

		this.restTemplate.postForObject(transferEndpoint, new HttpEntity<String>(transaction.toString(), headers),
				AccountTransaction.class);
	}

	private JSONObject createJSONAccountObject(Long accountNumber, Double amount) throws JSONException {
		JSONObject accountJSONObject = new JSONObject();
		accountJSONObject.put("accountNumber", accountNumber);
		accountJSONObject.put("balance", amount);
		return accountJSONObject;

	}

	private Account createAccount(JSONObject account) {
		return this.restTemplate.postForObject(accountEndpoint, new HttpEntity<String>(account.toString(), headers),
				Account.class);
	}

	private Account getAccount(Long accountNumber) {
		return this.restTemplate.getForObject(accountEndpoint + "/" + accountNumber, Account.class);
	}

}
