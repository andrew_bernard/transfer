# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ###

This is a Spring Boot 2.5 application
Java 8

there are three endpoints

### Create Account
curl -X POST \
  http://localhost:8080/account/ \
  -H 'content-type: application/json' \
  -d '{
	"accountNumber":"123",
	"balance":"100"
}'

#### Get account
curl -X GET \
  http://localhost:8080/account/123 \

### Transfer between accounts
curl -X POST \
  http://localhost:8080/account/transfer \
  -H 'content-type: application/json' \
  -d '{
	"source":"123",
	"destination":"456",
	"amount":"50"
}'



### How do I get set up? ###

mvn spring-boot:run 
* Configuration
* Dependencies
* Database configuration

Spring Boot H2 in memory Database
http://localhost:8080/h2-console
There 2 methods to get an account by the account number
One is read only the other uses pessimistic locking
Pessimistic Lock prevents other threads from tampering with selected accounts during the process of a transaction.
* How to run tests

mvn clean install

